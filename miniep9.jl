

function multiplica(a, b)
    dima = size(a)
    dimb = size(b)
    if dima[2] != dimb[1]
        return -1
    end
    c = zeros(dima[1], dimb[2])
    for i in 1:dima[1]
        for j in 1:dimb[2]
            for k in 1:dima[2]
            c[i, j] = c[i, j] + a[i, k] * b[k, j]
            end
        end
    end
    return c
end

function matrix_pot(a,p)
    a1 = a
    x = a
    if p == 1
        return a
    elseif p == 0
        return 1 
    elseif p >= 2
        for i in 1:p-1
            x = multiplica(x,a1)
        end
    end
    return x
end

function matrix_pot_by_squaring(M, p)
    return matrix_pot_by_squaring2(1, M, p)
end

function matrix_pot_by_squaring2(aux, M, p)
    if p == 0
        return  aux;
    elseif p == 1
        return  M * aux;
    elseif p % 2 == 0
        return matrix_pot_by_squaring2(aux, multiplica(M,M),  p / 2)
    else
        return matrix_pot_by_squaring2(M * aux, multiplica(M,M), (p - 1) / 2)
    end
end

using LinearAlgebra
function compare_times()
    M = Matrix(LinearAlgebra.I, 30, 30)
    @time matrix_pot(M, 10)
    @time matrix_pot_by_squaring(M, 10)
end
# compare_times()

using Test
function teste()
    @test matrix_pot([1 2 ; 3 4], 1) == matrix_pot_by_squaring([1 2 ; 3 4], 1) == [1 2 ; 3 4]^1
    @test matrix_pot([1 2 ; 3 4], 2) == matrix_pot_by_squaring([1 2 ; 3 4], 2) == [1 2 ; 3 4]^2
    @test matrix_pot([4 8 0 4 ; 8 4 9 6 ; 9 6 4 0 ; 9 5 4 7], 7) == matrix_pot_by_squaring([4 8 0 4 ; 8 4 9 6 ; 9 6 4 0 ; 9 5 4 7], 7) == [4 8 0 4 ; 8 4 9 6 ; 9 6 4 0 ; 9 5 4 7]^7
    println("Fim dos testes")
end
# teste()
